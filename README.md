# Dracula Today

The first version of this website was born during the second year of *Dracula Daily* newsletter, which presented *Dracula* by Bram Stoker in chronological order. The trouble was that you might never know which day is going to bring a new newsletter. Being a slightly impatient person who is not afraid of spoilers, I made website which checked whether to expect an email or not.

The initial website was very basic. When I returned a while later in order to improve the code, I ran across my own lack of skill. As I started learning more about HTML, the current version of design emerged.

The descriptions used in embeded links are collected [in a separate document](https://git.disroot.org/botanicals/DraculaToday/src/branch/main/descriptions.md).

Any issues spotted can be reported either through the repository or using [a form](https://cloud.disroot.org/apps/forms/s/YB9253WBBnJKzkDqMcbm2pfF).

## Copyright Notice
                
The text of the novel has been pulled from Standard Ebook's version, which has been released under the [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) license.

All JavaScript code is released under GPLv3 license. All other content is copyrighted to vedri and all rights are reserved.

The font used for the title of the website is [areminiscentsmile](https://www.behance.net/gallery/130853993/areminiscentsmile-Free-Elegant-Serif-Typeface) by Yoo Jihun. The font used for the rest of the headings and the entries is [Newsreader](https://github.com/productiontype/Newsreader) by Production Type. The font for the rest of the content is [SUIT](https://github.com/sunn-us/SUIT) by SUNN. All fonts are available for personal and commercial use.