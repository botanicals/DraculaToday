# The List of Descriptions for Each Date

## May

- May 3: Jonathan travels and the trains are natrually very late.
- May 4: Jonathan receives a rosary on the eve of the night when all evil things will have full sway.
- May 5: Jonathan gets an uncomfortable send-off on the last leg of his journey to the castle.
- May 7: Dracula would like language lessons to improve his already near perfect English.
- May 8: There is no man in the mirror, but Jonathan thinks there should be.
- May 9: Mina writes to Lucy about her learning goals.
- May 11: Lucy writes to Mina about her secret.
- May 12: Jonathan discusses the practices of hiring solicitors with the Count.
- May 15: Jonathan tries all the doors.
- May 16: Jonathan takes pleasure in disobeying the Count's warning.
- May 18: The door that Jonathan had been able to open before has been closed most forcefully.
- May 19: Jonathan is asked to write some letters.
- May 24: Lucy writes to Mina about her three proposals.
- May 25: Dr. Seward wallows by working too much and Quincey invites Arthur out to celebrate. 
- May 26: Art sends a telegram.
- May 28: Jonathan tries to send some letters.
- May 31: Jonathan finds himself without one scrap of paper.

## June

- June 5: Dr. Seward describes a curious activity of one of his patients.
- June 17: Jonathan is locked in while Count's hired workers are around.
- June 18: Dr. Seward's patient has stopped collecting flies.
- June 24: Jonathan spots the Count wearing his clothes.
- June 25: Jonathan climbs through some windows.
- June 29: Jonathan would like to leave immediately. Alas!
- June 30: Jonathan tries the doors again. 

## July 

- July 1: Renfield needs to get rid of the spiders.
- July 8: Renfield has a new pet.
- July 18: The captain of the Demeter backdates some logs.
- July 19: Renfield asks for a kitten.
- July 20: Dr. Seward concerns himself with Renfield starting anew in order not to think about Lucy.
- July 22: The Demeter is hit with some rough weather.
- July 24: The Demeter loses another man, and further north, Mina is visiting Lucy.
- July 26: Mina worries about Jonathan and Lucy's sleepwalking.
- July 27: Mina's worries haven't been alleviated.
- July 28: The Demeter's remaining men are worn out.
- July 29: The night watch on the Demeter has disappeared.
- July 30: The Demeter nears England.

## August

- August 1: Lucy and Mina learn about local legends, and the Demeter seems to be the only ship in the English channel.
- August 2: The Demeter loses another man.
- August 3: Mina continues to worry about Jonathan, and the mate has learned the secret of the Demeter.
- August 4: The captain remains the only man onboard the Demeter.
- August 6: Mina and the coastguard observe a ship that doesn't know her own mind.
- August 8: Mina sleeps poorly due to the storm and Lucy's sleepwalking, and The Dailygraph reports on the storm.
- August 9: The Dailygraph reports more details about the state of the Demeter, including a translation of her log.
- August 10: Mina and Lucy attend the funeral of the Demeter's captain.
- August 11: Mina wakes up to discover that Lucy has sleepwalked to their favourite seat across town.
- August 12: Mina is kept awake by Lucy, and a letter has been written in Budapest with long awaited news.
- August 13: Mina wakes up once again in the middle of the night.
- August 14: Mina starts worrying about Lucy too.
- August 15: Mina is privy to news, both public and secret, in the Westenra household.
- August 17: Mina notes that Lucy keeps getting weaker, and solicitors in Whitby arrange for a transfer of boxes.
- August 18: Lucy tells Mina what she remembers of her sleepwalked trip to their favourite spot last week.
- August 19: Mina receives the letter from Budapest, and Seward has an exciting night.
- August 20: Seward goes to assess Renfield after the previous night.
- August 21: Change is returned to solicitors in Whitby.
- August 22: Seward notes no changes in Renfield.
- August 23: Seward has yet another exciting night because of Renfield.
- August 24: Mina writes of happy news from Budapest, and Lucy starts keeping a diary.
- August 25: Lucy has another bad night.
- August 30: Lucy writes a brief letter to Mina to keep her up to date.
- August 31: Arthur writes to Seward for help with Lucy's failing health.

## September

- September 1: Arthur sends a telegram.
- September 2: Seward writes a status update to Arthur, and receives a letter himself from Van Helsing.
- September 3: Seward writes to Arthur an account of his and Van Helsing's visit with Lucy, as if writing for The Daily Telegraph.
- September 4: Seward is still very much interested in what Renfield is up to, and he writes a status update to Van Helsing.
- September 5: Yet another status update from Seward to Van Helsing.
- September 6: Seward telegrams an urgent call for help to Van Helsing and writes a more sober letter to Arthur.
- September 7: Seward and Van Helsing perform an urgent operation.
- September 8: Seward sits with Lucy through the night.
- September 9: Seward lets himself be talked out of sitting up tonight, and Lucy has recovered enough to write in her diary.
- September 10: Seward wakes up to find Lucy relapsed.
- September 11: Seward tries to make sense of Van Helsing's flowers, specially ordered.
- September 12: Lucy is comforted by the garlic flowers, however strange they might be.
- September 13: Seward watches Van Helsing breaking down after yet another one of his cures is thwarted.
- September 17: Seward gets assaulted by a patient, Van Helsing misdirects a telegram, Lucy has a run of four good days but is forced to write a note late at night, and Mina sends off a letter to Lucy full of good news.
- September 18: The Pall Mall Gazette's intrepid reporter finds out about an escaped wolf. Lucy's doctors come back in the nick of time. There's another death.
- September 19: Seward enumerates changes in Lucy.
- September 20: Seward gets a report of another escape, and later notes Lucy's troubles with a heavy heart.
- September 22: Mina and Jonathan return from a funeral to a telegram of another funeral, on the accounting of which Seward means to permanently end his diary.
- September 23: Mina declares her intention to read the sealed journal.
- September 24: Mina is shaken by what she has read, and Van Helsing writes an intriguing letter.
- September 25: The children are taken for an ominous walk during the night by a beautiful lady, and Mina arranges and then expands upon a visit from Van Helsing.
- September 26: Both Jonathan and Seward resume their diaries.
- September 27: Seward and Van Helsing undertake a grim task of confirming their theory, and Van Helsing leaves a just-in-case letter.
- September 28: In the light of day, Seward begins to doubt Van Helsing.
- September 29: Seward changes his mind again, due to Van helsing arguing for himself and providing hard proof. Later, Mina puts her typing skills to use and learns all, while Jonathan fills in the details of what happened at Whitby.
- September 30: As Jonathan and Mina put the various journal entries into chronological order, a pattern begins to emerge. Van Helsing presents a plan of attack to all present.

## October 

- October 1: Seward leads the assembled men to meet Renfield, who wants to leave the asylum, and then they attempt to act on their collective knowledge with the help of Arthur's hunting dogs. All that and the day has just started.
- October 2: Jonathan makes a crucial discovery while Mina still remains in the dark.
- October 3: Renfield reveals that the enemy has breached the lines. After the chaos of the morning, Mina is taken back into the confidence.
- October 4: Mina has a brilliant idea, and the hunt for the last box of earth begins.
- October 5: Mina keeps minutes of the meeting where Van Helsing reports what they have discovered about the box of earth during the day.
- October 6: Because Mina must come with them too, Van Helsing urges them to get ready for the journey.
- October 11: Mina sets a grim task before them the evening before the journey.
- October 15: Jonathan summarises their journey to Varna.
- October 16: No news, Jonathan notes.
- October 17: Everything is ready to welcome the Count, according to Jonathan.
- October 24: Jonathan notes that by all reports, the ship hasn't arrived yet. There's a telegram though.
- October 25: Seward describes everyone's impatience in waiting for the Count.
- October 26: Although it should have arrived, Seward has seen no sign of the long-awaited ship
- October 27: Seward is concerned by Mina's lethargy.
- October 28: Finally, the ship has been spotted. Mina outs herself as a train fiend.
- October 29: On the train, chasing the Count, Mina under hypnosis reaches the Count less easily than before.
- October 30: Sleuthing reveals how the Count has escaped his welcome. Mina goes over the papers and writes a memorandum.
- October 31: They separate to chase the Count, each group on its own path.

## November 

- November 1: Jonathan overhauls boats under Romanian flag, and Mina travels fast in a carriage.
- November 2: The searching has been fruitless so far.
- November 3: Seward has found a hint of the Count.
- November 4: Jonathan's launch has an accident and Van Helsing writes a memorandum in Mina's place.
- November 5: Van Helsing continues his accounting of their journey, doing all to cripple the enemy's forces.
- November 6: The final confrontation.

## Epilogue

- Epilogue: Seven years later, Jonathan returns to Romania.