// function that creates an entryDates
// list-compatible string
function dateString(moon, day) {

    let month;
    let date;

    if (moon < 10) {

        month = "0" + moon.toString();
    
    } else {
    
        month = moon.toString();
    
    }
    
    if (day < 10) {
    
        date = "0" + day.toString();
    
    } else {
    
        date = day.toString();
    
    }

    return month + "-" + date;

}

// function that turns entryDates list member 
// into a readable date
function listDate(listString) {

    const dateString = "2024-" + listString;

    const nextDate = new Date(dateString);
    const options = {
        month: "long",
        day: "numeric"
    }

    return nextDate.toLocaleDateString("en-US", options);

}

// function that finds the next date with entry
function findDate(month, date) {

    // exclude dates between the end and the new start
    if (month < 5 || month > 11 ||
        month === 5 && date < 3 ||
        month === 11 && date > 8) {

        return entryDates[0];

    }

    // search for the next date
    let dateIteration;

    let i;
    let j;

    find: for (i = month; i < 12; i++) {

        if (i === month) {

            j = date;

        } else {

            j = 1;
        }
        
        for (; j < 32; j++) {

            dateIteration = dateString(i, j);
            console.log(dateIteration);
            
            if (entryDates.indexOf(dateIteration) > -1){

                break find;

            }

        }

    }

    return dateIteration;

}

// dates on which there are entries in Dracula
const entryDates = ["05-03", "05-04", "05-05", "05-07", "05-08", "05-09", "05-11", "05-12", "05-15", "05-16", "05-18", "05-19", "05-24", "05-25", "05-26", "05-28", "05-31", "06-05", "06-17", "06-18", "06-24", "06-25", "06-29", "06-30", "07-01", "07-08", "07-18", "07-19", "07-20", "07-22", "07-24", "07-26", "07-27", "07-28", "07-29", "07-30", "08-01", "08-02", "08-03", "08-04", "08-06", "08-08", "08-09", "08-10", "08-11", "08-12", "08-13", "08-14", "08-15", "08-17", "08-18", "08-19", "08-20", "08-21", "08-22", "08-23", "08-24", "08-25", "08-30", "08-31", "09-01", "09-02", "09-03", "09-04", "09-05", "09-06", "09-07", "09-08", "09-09", "09-10", "09-11", "09-12", "09-13", "09-17", "09-18", "09-19", "09-20", "09-22", "09-23", "09-24", "09-25", "09-26", "09-27", "09-28", "09-29", "09-30", "10-01", "10-02", "10-03", "10-04", "10-05", "10-06", "10-11", "10-15", "10-16", "10-17", "10-24", "10-25", "10-26", "10-27", "10-28", "10-29", "10-30", "10-31", "11-01", "11-02", "11-03", "11-04", "11-05", "11-06"];

// parallel to entryDates,
// marks which dates have multiple entries
// 0 means only one entry, 1 means 2 or more entries
const multipleEntries = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0];

// get local date and turn it into entry string
const localDate = new Date();

let month = localDate.getMonth() + 1;
let date = localDate.getDate();

const today = dateString(month, date);

// target the #answer div for updates
const answer = document.querySelector("#answer");
const word = document.createElement("p");
const link = document.createElement("a");
const next = document.createElement("p");

next.setAttribute("class", "note");
let nextEntry;

// entry checking routine

if (today === "11-07") {

    // epilogue is linked on Nov 7
    // but it is officially day without an entry

    word.textContent = "NO";
    link.textContent = "Read the epilogue";
    link.setAttribute("class", "button");
    link.href = "entries/epilogue.html";
    next.textContent = "To read the next entry, return on May 3.";

} else {

    // check whether it's an entry day
    const entry = entryDates.indexOf(today);

    if (entry == -1) {

        // there are no entries

        word.textContent = "NO";
        link.textContent = "Check out the entry index";
        link.setAttribute("class", "button disabled");
        link.href = "entries/index.html";

        // find the next entry date
        nextInList = findDate(month, date);
        nextEntry = listDate(nextInList);
        nextIndex = entryDates.indexOf(nextInList);

        if (multipleEntries[nextIndex] === 0) {

            next.textContent = "To read the next entry, return on " + nextEntry + ".";

        } else {
            
            next.textContent = "To read the next entries, return on " + nextEntry + ".";

        }

    } else {
        
        if (multipleEntries[entry] === 0) {

            // there is only one entry

            word.textContent = "YES";
            link.textContent = "Read the entry";
            link.setAttribute("class", "button");
            link.href = "entries/" + today + ".html";

        } else {

            // there are multiple entries

            word.textContent = "YES";
            link.textContent = "Read the entries";
            link.setAttribute("class", "button");
            link.href = "entries/" + today + ".html";

        }

        // add a note on when to expect the next entry
        nextEntry = listDate(entryDates[entry + 1]);

        if (multipleEntries[entry + 1] === 0) {

            next.textContent = "To read the next entry, return on " + nextEntry + ".";

        } else {
            
            next.textContent = "To read the next entries, return on " + nextEntry + ".";

        }

    }

}

answer.appendChild(word);
answer.appendChild(link);
answer.appendChild(next);